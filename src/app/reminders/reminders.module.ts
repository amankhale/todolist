import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RemindersRoutingModule } from './reminders-routing.module';
import { ReminderListComponent } from './pages/reminder-list/reminder-list.component';
import { ReminderDetailsComponent } from './pages/reminder-details/reminder-details.component';


@NgModule({
  declarations: [
    ReminderListComponent,
    ReminderDetailsComponent
  ],
  imports: [
    CommonModule,
    RemindersRoutingModule
  ]
})
export class RemindersModule { }
